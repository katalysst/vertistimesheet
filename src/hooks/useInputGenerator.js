import React from 'react';

import DatePicker from "react-datepicker";
import TimePicker from 'rc-time-picker';

import 'rc-time-picker/assets/index.css';
import "react-datepicker/dist/react-datepicker.css";

export function useInputGenerator(fields, errors, styles) {
    let generatedInputs = [];
    let options = [];

    const format = 'h:mm a';
    
    fields.map( ( field, idx ) => { 
        let type = field.type;

        switch(type) {
                case 'select':
                  return  generatedInputs.push(
                            <div key={idx}>
                                <select value={field.state || ''} onChange={ e => field.func(e.target.value)}>
                                    <option value='' disabled hidden>Choose a {field.name}</option>
                                    { 
                                        field.selectOptions.map( (opt, ind ) => {
                                            options.push(<option key={ind} value={opt}>{opt}</option>)
                                            return true;
                                        })
                                        
                                    }
                                    
                                    {options}
                            
                                </select>
                                <p className={styles.error}>{errors.find(error =>  error.field === field.name ) !== undefined ? errors.find(error =>  error.field === field.name ).message : ''}</p>
                            </div>
                        );
                case 'text':
                case 'password' :
                  return generatedInputs.push(
                    <div key={idx}>
                        <input type={field.type} 
                        placeholder={`${field.name}`} 
                        onChange={ e => field.func(e.target.value)} 
                        value={field.state || ''} 
                        {...field.options} 
                        />
                        <p className={styles.error}>{errors.find(error =>  error.field === field.name ) !== undefined ? errors.find(error =>  error.field === field.name ).message : ''}</p>
                    </div>
                );
                case 'date':
                  return generatedInputs.push(
                    <div key={idx}>
                        <DatePicker selected={field.state}  onChange={ e => field.func(e)} onClose={e => field.func(e)} onSelect={ e => field.func(e)} value={field.state || ''} />
                        <p className={styles.error}>{errors.find(error =>  error.field === field.name ) !== undefined ? errors.find(error =>  error.field === field.name ).message : ''}</p>
                    </div>
                  );
                case 'time':
                  return generatedInputs.push(
                    <div key={idx}  onChange={ (e) => field.func(e)}>
                        <TimePicker 
                            showSecond={false} 
                            defaultValue={field.state} 
                            value={field.state}
                            onChange={ (e) => field.func(e)}
                            format={format}
                            use12Hours
                            inputReadOnly
                            onClose={e => field.func(field.state)}                            
                        />
                        <p className={styles.error}>{errors.find(error =>  error.field === field.name ) !== undefined ? errors.find(error =>  error.field === field.name ).message : ''}</p>
                    </div>
                  );
                case 'textarea':
                  return generatedInputs.push(
                    <div key={idx}>
                        <textarea placeholder={field.name} value={field.state || ''} onChange={ e => field.func(e.target.value)} ></textarea>
                        <p className={styles.error}>{errors.find(error =>  error.field === field.name ) !== undefined ? errors.find(error =>  error.field === field.name ).message : ''}</p>
                    </div>
                  );
                default:
                  return null;
              }

    });

    return generatedInputs;
}