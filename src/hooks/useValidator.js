import validator from 'validator';
import moment from 'moment';

export function useEmptyValidator(field) {
    return validator.isEmpty(field) ? `Field cannot be empty` : false ;
}

export function useDateValidator(start, end) {
    return validator.isBefore(moment(end).toString(), moment(start).toString()) ? 'End Time should be after the Start Time' : false;
}