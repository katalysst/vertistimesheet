import React from 'react';

import './styles/globals.scss';

import Login from './components/Login/Login';
import Timesheet from './components/Timesheet/Timesheet';

import { TimesheetProvider } from './TimesheetContext.js';

import useLocalStorage from './hooks/useLocalStorage';

function App() {

  const [employee, setEmployee] = useLocalStorage('User', '');


  const updateEmployee =  (emp) => {
    
    setEmployee(emp);
  };


  return (
    <TimesheetProvider value={{
      employee: employee,
      updateEmployee: updateEmployee
    }}>
    
      { !employee.hasOwnProperty('token') ?
        <Login />
      :
        <Timesheet />
      }
    </TimesheetProvider>
  );
}

export default App;
