import React, { useContext, useState } from 'react';

import TimesheetContext from './../../TimesheetContext';

import TimesheetForm from "./TimesheetForm";

import styles from './Timesheet.module.scss';

import timesheet from './../../images/timesheet.png';

export default function Timesheet() {

    const context = useContext(TimesheetContext);
    const [submitError, setSubmitError] = useState('');
    const [count, setCount] = useState(0);
    const [answers, setAnswers] = useState([]);
    const [reload, setReload] = useState(false);
    const [errors, setErrors] = useState([]);

    const submitTimesheet = (e, formAnswers, err) => {

        let ans = [...answers];
        let answerFound = ans.find(answer => answer.formID === formAnswers.formID);

        if (answerFound !== undefined) {
            ans[ans.indexOf(answerFound)] = formAnswers;
        } else {
            ans.push(formAnswers);
        }
        setAnswers(ans);

        setErrors([...errors, ...err]);
    }


    const submitForm = (e) => {
        e.preventDefault();

        let noErrors = errors.filter(error => error.message === "");

        if (noErrors !== undefined && noErrors.length === errors.length) {
            answers.map(answer => {
                fetch('https://vertisbackend.herokuapp.com/api/timesheet', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `JWT ${context.employee.token}`,
                    },
                    body: JSON.stringify(answer.answers)
                }).then(response => response.json()).then(resp => {
                    if (resp.status === 200) {
                        setReload(true)
                    }
                }).catch(error => console.error('Error:', error));
                return true;
            })
        } else {
            setSubmitError('Please answer all fields before submit');
        }
    }

    const generateFields = () => {
        let sheets = [];

        for (let i = 0; i < count + 1; i++) {
            sheets.push(<TimesheetForm key={i} submitTimesheet={submitTimesheet} formID={i} setErrors={setErrors} errors={errors} />);
        }
        return sheets;

    }

    const totalTime = () => {
        let total = 0;

        answers.map(answer => {
            total = total + answer.totalTime
            return true;
        })

        return total;
    }


    return (
        <div id={styles.timesheet}>

            {reload ?
                <div className={styles.success__side}>

                    <img src={timesheet} alt="vector of a calendar and clock" />
                    <h2>Good Job {context.employee.firstName}! Timesheet has been saved</h2>
                    <button className={styles.add__more__button} onClick={() => { setReload(false) }}>Add More</button>
                    <button className={styles.add__more__button} onClick={() => {context.updateEmployee({})}}>Logout</button>
                </div>
                :

                <span>
                    <div className={styles.top__side}>
                        <img src={timesheet} alt="vector of a calendar and clock" />
                        <h2>Welcome {context.employee.firstName}! Let's get started on your timesheet</h2>
                        <i className="fas fa-chevron-circle-down"></i>
                        <button className={styles.add__more__button} onClick={() => {context.updateEmployee({})}}>Logout</button>
                        
                    </div>
                    <div className={styles.bottom__side}>

                        {generateFields()}

                        <div className={styles.total}><p>Total Time Spent {totalTime()} </p></div>
                        <p className={`${styles.error} ${styles.form__error}`}>{submitError}</p>
                        <div className={styles.time__button}>

                            <div>
                                <button className={styles.add__time} onClick={() => setCount(count + 1)}><i className="fas fa-plus"></i></button>
                                <p>Add More</p>
                            </div>
                            <div>
                                <button className={styles.save} onClick={(e) => submitForm(e)}><i className="far fa-save"></i></button>
                                <p>Save</p>
                            </div>
                        </div>
                    </div>
                </span>
            }

        </div>
    )
}