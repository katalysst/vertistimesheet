import React, { useContext, useState, useEffect } from 'react';
import moment from 'moment';

import TimesheetContext from './../../TimesheetContext';

import { useInputGenerator } from '../../hooks/useInputGenerator';
import { useEmptyValidator, useDateValidator } from './../../hooks/useValidator';

import styles from './Timesheet.module.scss';


export default function TimesheetForm(props) {

    const now = moment().hour(0).minute(0);

    const context = useContext(TimesheetContext);

    const formID = props.formID;

    const [clients, setClients] = useState([]);
    const [currentClient, setCurrentClient] = useState([]);
    const [workTitle, setWorkTitle] = useState('');
    const [clientName, setClientName] = useState('');
    const [date, setDate] = useState(Date.now());
    const [startTime, setStartTime] = useState(now);
    const [endTime, setEndTime] = useState(now);
    const [comments, setComments] = useState('');
    const [employeeName, setEmployeeName] = useState('');
    const [employeeAddress, setEmployeeAddress] = useState('');
    const [managersName, setManagersName] = useState('');
    const [managersPosition, setmanagersPosition] = useState('');
    const errors = props.errors;

    const fields = [
        { name: 'Work Title', func: setWorkTitle, type: 'text', state: workTitle },
        { name: 'Client\'s Name', func: setClientName, type: 'select', state: clientName, selectOptions: clients.map(client => client.ClientName) },
        { name: 'Date', func: setDate, type: 'date', state: date },
        { name: 'Start Time', func: setStartTime, type: 'time', state: startTime },
        { name: 'End Time', func: setEndTime, type: 'time', state: endTime },
        { name: 'Comments', func: setComments, type: 'textarea', state: comments },
        { name: 'Employee\'s Name', func: setEmployeeName, type: 'text', state: employeeName, options: { 'readOnly': true, 'value': `${context.employee.firstName} ${context.employee.lastName}` } },
        { name: 'Employee\'s Address', func: setEmployeeAddress, type: 'text', state: employeeAddress, options: { 'readOnly': true, 'value': context.employee.address } },
        { name: 'Manager\'s Name', func: setManagersName, type: 'text', state: managersName, options: { 'readOnly': true, value: managersName || '' } },
        { name: 'Manager\'s Position', func: setmanagersPosition, type: 'text', state: managersPosition, options: { 'readOnly': true, value: managersPosition || '' } },
    ];

   

    const isWorkTitleEmpty = useEmptyValidator(workTitle);
    const isClientNameEmpty = useEmptyValidator(workTitle);
    const isEndTimeBefore = useDateValidator(startTime, endTime);
    const isCommentsEmpty = useEmptyValidator(comments);
    
   


    useEffect(() => {
        const getClients = (() => {

            fetch('https://vertisbackend.herokuapp.com/api/clients', {
                method: 'GET',
                headers: { 'Authorization': `JWT ${context.employee.token}` },
            }).then(response => response.json()).then(resp => {
                if (resp.status === 200) {
                    setClients(resp.clients)
                }
            }).catch(error => console.error('Error:', error));
    
        });
       
    
        getClients();
        
        let errors = [
            { 'field': 'Work Title', 'message': isWorkTitleEmpty !== false ? isWorkTitleEmpty : '' },
            { 'field': 'Client\'s Name', 'message': isClientNameEmpty !== false ? isClientNameEmpty : '' },
            { 'field': 'End Time', 'message': isEndTimeBefore !== false ? isEndTimeBefore : '' },
            { 'field': 'Comments', 'message': isCommentsEmpty !== false ? isCommentsEmpty : '' },
        ]

        props.setErrors(errors);
    }, [isClientNameEmpty, isWorkTitleEmpty, isEndTimeBefore, isCommentsEmpty, context.employee.token])


    const generatedInputs = useInputGenerator(fields, errors, styles);


    useEffect(() => {
        const setManager = (() => {
            setCurrentClient(clients.find(client => client.ClientName === clientName));
            if (currentClient !== undefined) {
                setManagersName(currentClient.ManagerName);
                setmanagersPosition(currentClient.ManagerPosition);
            }
        })
        setManager();

        answers.totalTime = parseInt(totalTime());

    })

    const controlErrors = (() => {

        let errors = [
            { 'field': 'Work Title', 'message': isWorkTitleEmpty !== false ? isWorkTitleEmpty : '' },
            { 'field': 'Client\'s Name', 'message': isClientNameEmpty !== false ? isClientNameEmpty : '' },
            { 'field': 'End Time', 'message': isEndTimeBefore !== false ? isEndTimeBefore : '' },
            { 'field': 'Comments', 'message': isCommentsEmpty !== false ? isCommentsEmpty : '' },
        ]

        props.setErrors(errors);
    })

    const milisecondsToHours = (d) => {
        return (d / (3.6e+6)).toFixed(2)
    }

    const totalTime = () => {
        return milisecondsToHours(moment(endTime).toDate() - moment(startTime).toDate());

    }

    const answers = {
        formID,
        answers:
            {
                workTitle, clientName, date: moment(date).format("YYYY-MM-DD HH:mm:ss"), startTime: moment(startTime).format("hh:mm:ss"), endTime: moment(endTime).format("hh:mm:ss"), comments, clientID: currentClient !== undefined ? currentClient.ClientID : 0, employeeID: context.employee.employeeID
            },
        totalTime: 0

    }

    return (
        <form onChange={(e) => props.submitTimesheet(e, answers, errors)} onClick={e => controlErrors()}>
            {generatedInputs}
        </form>
    )
}