import React, { useState, useContext } from 'react';

import TimesheetContext from './../../TimesheetContext';

import { useEmptyValidator } from './../../hooks/useValidator';
import { useInputGenerator } from './../../hooks/useInputGenerator';

import styles from './Login.module.scss';

import Timesheet from '../Timesheet/Timesheet';


function Login () {

    const context = useContext(TimesheetContext);
    
    const [redirect, setRedirect] = useState(false);
    const [UserName, setUserName] = useState('');
    const [Password, setPassword] = useState('');
    const [errors, setErrors] = useState([]);

    const fields =  [ 
        { name: 'Username', func: setUserName, type: 'text', state: UserName },
        { name: 'Password', func: setPassword, type: 'password', state: Password },
    ];

    const isEmailEmpty = useEmptyValidator(UserName);
    const isPasswordEmpty = useEmptyValidator(Password);

    const generatedInputs = useInputGenerator(fields, errors, styles)

    const submitForm = (e) => {
        e.preventDefault();

        let errors = [
            {'field': 'Username', 'message': isEmailEmpty !== false ? isEmailEmpty : ''},
            {'field': 'Password', 'message': isPasswordEmpty !== false ? isPasswordEmpty : ''},
        ]

       
        let noErrors = errors.filter(error => error.message === "");

        if (noErrors !== undefined && noErrors.length === errors.length) {
            submitRequest();
        }

        setErrors(errors);        
    }

    const submitRequest = () => {
        fetch('https://vertisbackend.herokuapp.com/api/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({UserName, Password})
        }).then( response => response.json()).then( resp => {
                if (resp.status === 200) {
                    context.updateEmployee({token: resp.token, employeeID: resp.employee.employeeID, firstName: resp.employee.firstName, lastName: resp.employee.lastName, address: resp.employee.address})
                    setRedirect(true);
                } else {

                }
        }).catch(error => console.error('Error:', error));
    }
    

    return (
        redirect ? 
            <Timesheet /> 
        : 
            <div id={styles.login}>
                <div className={styles.form__container}>
                    <div className={styles.form}>
                        <h1 className={styles.highlight} >Welcome to the Family</h1>
                        <p>Already have an account? <span className={styles.highlight}>Login</span></p>
                        
                        <form>
                            {generatedInputs}

                            
                        <div>
                                <input type="checkbox" /> <span>I have read the <span className={styles.highlight}>terms and conditions</span></span>
                            </div>
                            <button onClick={e => submitForm(e)}>Let's Get Started</button>
                        </form>
                    
                    </div>
                </div>
            </div>
    )
}

export default Login