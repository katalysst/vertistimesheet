import React from 'react';

const TimesheetContext = React.createContext();

export const TimesheetProvider = TimesheetContext.Provider
export const TimesheetConsumer = TimesheetContext.Consumer
export default TimesheetContext